require 'cuba'
require 'json'
require './stores/memory_store'

store = Store::MemoryStore.new

Cuba.define do

  on root do
    res.write('Welcome to our minimal (and very useless) persistence layer')
  end

  on get do
    on "register" do
      uuid = store.register
      res.headers["Content-Type"] = "application/json"
      res.write ({id: uuid}).to_json
    end

    on ":type" do |type|
      uuid = env["HTTP_AUTHORIZATION"]
      results = store.get uuid: uuid,
                          type: type
      res.headers["Content-Type"] = "application/json"
      res.status = 200
      res.write results.to_json
    end
  end

  on post do
    on ":type" do |type|
      uuid = env["HTTP_AUTHORIZATION"]
      value = req.body.read
      store.add uuid: uuid,
                type: type,
                value: JSON.parse(value)
      res.status = 204
      res.finish
    end
  end
end
