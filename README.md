# Persistence Abstraction microservice
Minimal/naive persistence abstraction service.

## API
This microservice exposes a very minimal and naive HTTP-based API:

    GET /register => returns a unique id that the client can use
    POST /:type => Adds a new item to under `type` (resource type)
    GET  /:type => Returns the collection

Both `POST /:type` and `GET /:type` require an Authorization header
(a unique id) that will be used as the storage namespace. Since this
is only a test, there's no real checking that the unique id was
registered: if you supply a random/custom unique id, the service will
create a namespace for you.

## Store abstraction
The only store available is the MemoryStore. All stores respond to a
basic interface: `register`, `get`, and `add` which will create, get,
and add storage namespaces respectively.

The `Store::MemoryStore` implementation simply saves everything in an
instance variable (`@store`) which is implemented as a hash:
`{ 'uid' => [array], 'uid2' => [array]}`.

## Notes

I've used [Cuba](http://cuba.is/) since it's just a layer on top of Rack.
I've used [gs](https://github.com/soveran/gs) instead of bundler together with
[dep](https://github.com/cyx/dep) to keep it simple. To install
both gems and then install the dependencies:

```
gem install gs
gem install dep
gs init
gs dep install
```

From here, to start the application, simply run `gs rackup config.ru`.

## Tests
`cuba/test` is basically [cutest](https://github.com/djanowski/cutest)
and `Rack::Test`, so I've used those in the tests. I've only tested the
HTTP api (no unit tests for the memory store). In theory, any store that
passes the HTTP API tests should be correct, so to keep the ordeal short
tests have been kept intentionally brief.

To run the tests:

```
gs ruby test/app_test.rb
```
