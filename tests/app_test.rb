require 'cuba/test'
require './app.rb'
require 'json'

scope do
  test '/' do
    get '/'
    assert_equal 'Welcome to our minimal (and very useless) persistence layer', last_response.body
  end

  test '/register' do
    get '/register'
    response = JSON.parse last_response.body
    assert response["id"].length == 36 # uuid length is 36
  end

end

scope "adding items" do
  setup do
    get "/register"
    response = JSON.parse last_response.body
    @uuid = response["id"]
  end

  test "POST /:type" do
    header "Authorization", @uuid
    user = {id: 1, name: 'John'}
    post "/users", user.to_json
    assert_equal last_response.status, 204
  end
end

scope "fetching items" do
  setup do
    get "/register"
    response = JSON.parse last_response.body
    @uuid = response["id"]
    @user1 = {id: 1, name: 'John'}
    @user2 = {id: 2, name: 'Jane'}
    header "Authorization", @uuid
    post "/users", @user1.to_json
    post "/users", @user2.to_json
  end

  test "GET /:type returns existing users" do
    header "Authorization", @uuid
    get "/users"
    res = JSON.parse last_response.body
    assert_equal res.length, 2
    assert_equal res.first.to_json, @user1.to_json
    assert_equal res.last.to_json, @user2.to_json
  end
end
