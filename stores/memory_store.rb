require 'securerandom'
require 'json'

module Store
  class MemoryStore
    def initialize
      @store = {}
    end

    def register
      uuid = SecureRandom.uuid
      @store[uuid] = []
      uuid
    end

    # saves/adds a value if it doesn't exist yet
    # I've kept it quite simple; if the namespace does not exist,
    # it will be created
    def add(uuid:, type:, value:)
      @store[uuid] ||= []
      namespace = @store[uuid]
      hash = {type: type, value: value}
      if namespace.include?(hash)
        return value
      else
        namespace.push(hash)
      end
      value
    end

    # Not exposed in the HTTP API yet, but here anyway
    def rem(uuid:, type:, value:)
      namespace = @store[uuid]
      return unless namespace
      namespace.delete({type: type, value:value})
    end

    # retreives all the values stored under this key
    # (sorry, no filtering)
    def get(uuid:, type:)
      results = find_by_type(uuid: uuid, type: type)
      if results
        results.map { |result| result[:value] }
      else
        nil
      end
    end

    private
    def find_by_type(uuid:, type:)
      arr = @store[uuid]
      return nil unless arr
      arr.select { |item| item[:type] == type}
    end
  end
end
